#include <iostream>

int
main()
{
    double piSeries = 0, pi = 3.14159, odd = 1, epsilon = 0.000001;
    int member = 1, memberSign = 1; 
    for (int accuracy = 100; accuracy < 1000000; accuracy *= 10) {
        int piInteger = static_cast<int>(pi * accuracy);
        double currentPi = static_cast<double>(piInteger) / accuracy;
        for (; (piSeries - currentPi) >= epsilon || (currentPi - piSeries) >= epsilon; odd += 2) {
       	    piSeries += memberSign * 4 / odd;
            memberSign *= -1;
            ++member;
	}
        std::cout << member << " - " << piSeries << std::endl;
    }
    return 0;
}

