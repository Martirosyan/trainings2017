#include <iostream>
#include <iomanip>

int 
main()
{
    int hours = 0;

    while (hours != -1) {
        std::cout << "\nEnter hours worked (-1 to end): ";
        std::cin  >> hours;
        if (-1 == hours) {
            return 0;
        }
        if (hours < 0) {
            std::cerr << "Error 1: Hours cannot be negative!" << std::endl;
            return 1;
        }

        double hourlyRate;
        std::cout << "Enter hourly rate of the worker ($00.00): $";
        std::cin  >> hourlyRate;
        if (hourlyRate < 0) {
            std::cerr << "Error 2: Hourly rate of the worker cannot be negative!" << std::endl;
            return 2;
        }

        double salary = hours * hourlyRate;
        if (hours > 40) {
            salary += (hourlyRate / 2) * (hours - 40);
        }
        std::cout << "Salary is: $"
                  << std::setprecision(2)
                  << std::fixed
                  << salary << std::endl;
    }
    return 0;
}
