The purpose of this program -> cout << ++(x+y) is to add x and y (x+y) and then add 1 to the sum of x and y(++(x+y)). In order to get the wanted result we should implement that code in the following way:

int 
main()
{
    int x = 1;
    int y = 1;
    int z = x + y;

    while (z != 10) {
        std::cout << ++z << std::endl;
    }
    return 0;
}
