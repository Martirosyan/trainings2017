#include "Employee.hpp"
#include <iostream>
#include <string>

Employee::Employee(std::string firstName, std::string lastName, int salary)
{
    setFirstName(firstName);
    setLastName(lastName);
    setMonthSalary(salary);
}

void
Employee::setFirstName(std::string firstName)
{
    firstName_ = firstName;
} 

std::string Employee::getFirstName()
{
    return firstName_;
}

void
Employee::setLastName(std::string lastName)
{
    lastName_ = lastName;
}

std::string Employee::getLastName()
{
    return lastName_;
}

void
Employee::setMonthSalary(int salary)
{
    if (salary < 0) {
        monthSalary_ = 0;
        std::cout << "\tInfo 1: Salary cannot be negative => it is automatically setted to 0" << std::endl;
        return;
    }
    monthSalary_ = salary;
}

int
Employee::getMonthSalary()
{
    return monthSalary_;
}
