a) Which logical unit of the computer receives information from outside the computer for use by the computer?  __ Input Unit

b) The process of instructing the computer to solve specific problems is called COMPUTERS PROGRAMS. These programs guide the computer through orderly sets of actions specified by people called computer programmers.

c) What type of computer language uses English-like abbreviations for machine-language instructions?  __ Assembler Languages

d) Which logical unit of the computer sends information that has already been processed by the computer to various devices so that the information may be used outside the computer?  __ Output Unit 

e) Which logical unit of the computer retains information?  __ Secondary storage Unit 
f) Which logical unit of the computer performs calculations? __ ALU.
g) Which logical unit of the computer makes logical decisions? __ ALU.
h) The level of computer language most convenient to the programmer for writing programs quickly and easily is __ high-level languages
i) The only language that a computer directly understands is called that computer's __ machine language
j) Which logical unit of the computer coordinates the activities of all the other logical units? __ CPU
