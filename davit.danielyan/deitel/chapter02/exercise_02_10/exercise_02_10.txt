a)false-because actions are performed from left to right = (right to left);
b)b. True - but we should avoid variable names like _under_bar_, m928134, t5, j7 and so on.
c)c. False - typical example of an assignment statement is a = 5.
d)false - C ++ applies the operators in arithmetic expressions in a precise sequence determined by the rules of operator precedence.
e)True - Though h22 is started from letter, but we should avoid variable names like this.
