#include <iostream>

int
main() 
{ 
    int number1, number2;
    std::cout << "Type numbers: ";
    std::cin >> number1 >> number2;
        
    if (0 == number2) {
        std::cout << "Error 1: The second number can`t be zero" << std::endl;
        return 1;    
    } 

    if (0 == number1 % number2) {
        std::cout << number1 << " is a multiple of " << number2 << std::endl;
        return 0;
    }
    std::cout << number1 << " isn't a multiple of " << number2 << std::endl;
				    
    return 0;				       
}

