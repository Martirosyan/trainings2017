#include "Invoice.hpp" 
#include <iostream> 
#include <string>

int
main()
{
    std::string number;
    std::string description;
    int quantity = 0;
    int price = 0;
    
    Invoice invoice(number, description, quantity, price);
    
    std::cout << "Number: ";
    std::getline(std::cin, number);
    invoice.setNumber(number);
    
    std::cout << "Description: ";
    std::getline(std::cin, description);
    invoice.setDescription(description);
              
    std::cout << "Quantity: ";
    std::cin  >> quantity;
    invoice.setQuantity(quantity);
        
    std::cout << "Price: ";
    std::cin  >> price;
    invoice.setPrice(price); 
    
    std::cout << "Overall amount: " << invoice.getInvoiceAmount() << std::endl; 
    
    return 0; 
} 
