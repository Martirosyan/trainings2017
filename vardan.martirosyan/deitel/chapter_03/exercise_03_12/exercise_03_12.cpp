#include "Account.hpp"
#include <iostream>

int
main()
{
    int initial = 0;
    int addBalance = 0;
    int addDebit = 0;

    std::cout << "Account1: Enter your current: ";
    std::cin  >> initial;
    Account account1(initial);
    
    std::cout << "Account2: Enter your current balance: ";
    std::cin  >> initial;
    Account account2(initial);
    std::cout << "\nThe results of credit\n" << std::endl;
    
    std::cout << "Account1: Enter the credit: ";
    std::cin  >> addBalance;
    account1.credit(addBalance);
    std::cout << account1.getBalance(addBalance) << std::endl;
    
    std::cout << "Account2: Enter the credit: ";
    std::cin  >> addBalance;
    account2.credit(addBalance);
    std::cout << account2.getBalance(addBalance) << std::endl;

    std::cout << "\nThe results of debit\n" << std::endl;
    
    std::cout << "Account1: Enter the debit: ";
    std::cin  >> addDebit;
    account1.debit(addDebit);
    std::cout << account1.getBalance(addDebit) << std::endl;
    
    std::cout << "Account2: Enter the debit: ";
    std::cin  >> addDebit;
    account2.debit(addDebit);
    std::cout << account2.getBalance(addDebit) << std::endl;

    return 0;    
}
