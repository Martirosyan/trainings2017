#include <iostream>
int

main()
{
   int number1, number2, number3;

    std::cout << "Enter three numbers " << std::endl;
    std::cin >> number1 >> number2 >> number3;

    std::cout << "Sum is " << number1 + number2 + number3 << std::endl;
    std::cout << "Average is " << (number1 + number2 + number3) / 3 << std::endl;    
    std::cout << "Product is " << number1 * number2 * number3 << std::endl;

    ///small
    int small = number1;
    if (number2 < small) {
        small = number2;
    }
    if (number3 < small) {
        small = number3;
    }
    std::cout << small << std::endl;

    ///large
    int large = number1;
    if (number2 > large) {
        large = number2;
    }
    if (number3 > large) {
        large = number3;
    }
    std::cout << large << std::endl;
    
    return 0;
}

