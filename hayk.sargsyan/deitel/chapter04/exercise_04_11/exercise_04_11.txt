a) - 

if (age >= 65) {
    std::cout << "Age is greater than or equal to 65" << std::endl;
} else {
    std::cout << "Age is lower than 65" << std::endl;
}


b) - 

if (age >= 65) {
    std::cout << "Age is greater than or equal to 65" << std::endl;
} else {
    std::cout << "Age is lower than 65" << std::endl;
}


c) -

int x = 1;
int total = 0;

while (x <= 10) {
    total += x;
    ++x;
}


d) -

int x = 1;
int total = 0;

while (x <= 100) {
    total += x;
    ++x;
}


e) - 

int y = 100;

while (у > 0) {
    std::cout << у << std::endl;
    --y;
}
