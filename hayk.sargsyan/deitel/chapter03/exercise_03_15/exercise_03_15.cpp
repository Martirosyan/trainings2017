#include <iostream>
#include "Date.hpp"

int
main()
{
    Date date1(1999, 12, 11);
    Date date2(1969, 777, 30);

    std::cout << date1.getYear() << "/" << date1.getMonth() << "/" << date1.getDay() << std::endl;
    std::cout << date2.getYear() << "/" << date2.getMonth() << "/" << date2.getDay() << std::endl;

    return 0;
}
