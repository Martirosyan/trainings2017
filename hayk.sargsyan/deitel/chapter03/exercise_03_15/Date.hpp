#include <string>

class Date
{
public:
    Date(int year, int month, int day);
    void setYear(int year);
    void setMonth(int month);
    void setDay(int day);
    int getYear(void);
    int getMonth(void);
    int getDay(void);
private:
    int year_;
    int month_;
    int day_;
};

