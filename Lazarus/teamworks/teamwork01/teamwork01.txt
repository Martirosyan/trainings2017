
You are probably wearing on your wrist one of the world's most common types of objects a watch. Discuss how each of the following terms and concepts applies to the notion of a watch: object, attributes, behaviors, class, inheritance (consider, for example , analarm clock), abstraction, modeling, messages, encapsulation, interface , information hiding, data members and member functions.

Object - each exemplar of watch is an object.
Attributes are model, color and material of strap, material of clock (metal, wooden, plastic), size and its type (analog, digital).
Behaviors - it works correct or not correct.
Class - wrist watch is a class, containing different models of watches.
Inheritance - alarm clock inherits features of watch and adds its own ones (alarm).
Abstraction - it is an accessory, which shows time and is worn on wrist.
Modeling is describing functionality and the look of the watch.
Messages - the watch informs us about the time. 
Encapsulation - you can set up and change time with the help of keys.
Interface - it shows time on the display. We use buttons to change the time.
information hiding - it isn't necessary for a user to know the structure of the watch.
data members - hours, minutes and seconds.
Member function - setting time, showing the time and changing.

